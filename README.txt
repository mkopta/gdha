Tento malý projekt řeší následující zadání:

 Navrhnete (must) a pripadne naimplementujte (might) HA setup pro 2 sluzby na
 portech 3128 bezicich ve 2 virtualnich strojich na AWS v rezimu
 active/passive.
 Tento rezim znamena, ze pozadavky se primarne smeruji na jeden VM, v pripade
 jeho vypadku se zacnou pozadavky smerovat na zalozni VM.
 V pripade vyuziti AWS-specifickych technologii navrhnete implementaci take v
 ciste virtualizovanem prostredi (KVM).


Řešení
======

Základní princip je využití speciálního serveru, který bude distribuovat
požadavky mezi oba virtuální servery poskytující službu.


                            ext. IP
                              |
                            +----+
                            |    |
                            | HA |
                            |    |
                            +----+
                              |
                        +-----+------+
                        |            |
                     +----+        +----+
                     |    |        |    |
                     | B1 |        | B2 |
                     |    |        |    |
                     +----+        +----+
                     Backend       Backend


Řešení se sestává z tří částí.

  1) Data
  2) Virtuální servery poskytující službu
  3) Load balancing (LB) / High availability (HA) server

Zadání vyžaduje implementaci s využitím AWS. AWS nevyužívám, ale vytvořil jsem
si účet a pokusil se vytvořit si několik virtuálních serverů, na kterých bych
mohl provést implementaci HA. Pro mou neznalost AWS se mi to bohužel nepodařilo
a virtuální servery jsem si tak vytvořil ručně na pracovním počítači s využitím
qemu a kvm. Veřím, že řešení je dostatečně obecně platné i pro využití v AWS.

Data
----

Virtuální servery poskytující službu (backend servery) mohou být bezdiskové
stanice sloužící pouze jako výpočetní uzly. V takovém případě není potřeba
řešit data nijak.

Pokud backendy sdílejí data, je potřeba zařídit společné datové úložiště nebo
synchronizaci dat.

Je možné použít například

  1) NFS
  2) OCFS2 + DRBD

V případě použití NFS budeme potřebovat další (virtuální) server, tedy další
zdroje. Nastavení a použití NFS je velmi jednoduché a NFS poskytuje relativně
dobrý výkon a snadnou rozšiřitelnost.

V případě využití OCFS2 (nebo podobného clusterovaného FS) a DRBD není potřeba
vytvářet další virtální servery speciálně pro data, ale konfigurace je
náročnější a řešení nelze snadno rozšiřovat o další uzly (kvůli DRBD).

Zadání se o datech nezmiňuje a budu tedy uvažovat diskless stanice bootující z
PXE nebo vlastního disku. Rozšíření o NFS je triviální (exportfs, nfsd, 2x
mount, fstab, hard, intr).

Virtuální servery poskytující službu
------------------------------------

Ze zadání budu uvažovat libovolné systémy poskytující nějakou textovou službu
na nějakém konkrétním portu. Pro implementaci jsem si zvolil GNU/Linux Debian s
nginx serverem, protože jde o malý a rychlý systém, který se hodí pro testování
i servery. Samozřejmě lze použít libovolný jiný OS / server.

HA server
---------

Nejzajímavější částí řešení je řešení HA serveru, který rozděluje požadavky na
jednotlivé backendy. Pro implementaci LB/HA lze použít několik řešení, z nichž
jsem vybral dvě.

  1) nginx
  2) ha-proxy

Nginx umí fungovat jako reverzní proxy s load-balancingem, který lze jednoduše
zapnout do módu backup nebo round-robin. Nginx se ale hodí spíše pro HTTP(S)
provoz a ne pro libovolnou TCP/IP službu.

HA Proxy je schopná smysluplně rozdělovat požadavky mezi všechny definované
backendy libovolným způsobem, včetně požadovaného active/passive. Umí zjišťovat
stav jednotlivých backend serverů a podle toho přepínat rozesílání požadavků.
Hodí se pro libovolný TCP/IP provoz.

Pro řešení HA setupu jsem se rozhodl využít HA Proxy, protože je více určena
pro požadovaný typ úlohy a navíc nevím, jestli je služba HTTP nebo jiná.

Vysokou dostupnost samotného HA serveru lze řešit několika způsoby.

  A) DNS Round Robin + několik identických HA serverů.
  B) Předpřipravené kopie HA serveru přepínané pomocí DNS aktualizace.
  C) Častý backup HA serveru a připravený postup obnovy.
  D) V případě využití AWS -- Elastic Load Balancer.

V případě využití DNS Round Robin je potřeba míte více HA serverů, které
vykonávají stejnou práci. Pro malé zatížení jde o zbytečné plýtvání zdroji a
navíc DNS Round Robin neumí reflektovat stav jednotlivých HA serverů. Pokud tak
budeme mít dva HA servery v DNS RR a jeden vypadne, 50% požadavků selže. Pokud
to však odesílateli požadavků nevadí (může jít o automatizovaný systém, který
se prostě zeptá znovu), lze toto řešení použít.

Zjednodušená varianta předchozího řešení vyřazuje použití DNS RR a
přepřipravené HA servery nejsou využívané až do výpadku primárního. Poté je
potřeba provést ruční zásah, aktualizovat DNS záznamy a přesměrovat službu na
alternativní HA server. Toto řešení má zásadní nevýhodu v tom, že vyžaduje
ruční zásah a navíc vytvoří výpadek v řádu desítek minut (dle konfigurace DNS).

Úplně nejjednodušší řešení je HA proxy mít zálohovanou (častou repliku, nebo
připravený alternativní čistý systém) a případě potřeby mít možnost HA server
znovu nahodit ze záloh. Pokud HA server nemá žádnou speciální konfiguraci
(caching atp.), dá se počítat s výpadkem několika desítek minut. Dokonce i v
případě neexistence záloh je možné vytvořit nový HA server i do jedné hodiny.
Samozřejmě je nevýhodou potřeba manuálního zásahu.

Řešení dostupnosti samotného HA serveru je závislé na tom, jaké typy problémů
očekáváme a jak velké výpadky jsme ochotni podstoupit. Protože tyto požadavky
nejsou součástí řešení, budu uvažovat případ A, kde prostě jen vytvoříme
několik on-line kopií HA serveru a nastavíme Round Robin v DNS záznamu domény.

Implementace
============

Příprava prostředí
------------------

Nejprve si připravím tři virtuální servery na nichž poté rozchodím HA. Začnu s
vytvořením virtálního disku.

  qemu-img create -f qcow2 deb.qcow2 10G

Virtuální disk jsem zvolil typu qcow2, protože umožňuje thin-provisioning,
snapshoty a jde o nativní Qemu formát virtuálního disku. Velikost jsem zvolil
10G, což je dostatečná kapacita pro operační systém.

Do virtuálního disku nainstaluji OS GNU/Linux Debian.

  kvm -m 1024 -hda deb.qcow2 -cdrom debian-install.iso -boot d \
      -vnc :1 -monitor stdio

Po dokončení instalace si připravím na hostiteli virtuální síť pro propojení
virtuálních serverů. Vytvořím tři tap zařízení, které zapojím do bridge (a tím
do lokální sítě), který si vytvořím.

  # vytvoření tap devices
  tunctl -u root -t vnet0
  tunctl -u root -t vnet1
  tunctl -u root -t vnet2

  # vytvoření bridge
  brctl addbr br0

  # zapojení tap devices a fyzického iface do bridge
  brctl addif br0 eth0
  brctl addif br0 vnet0
  brctl addif br0 vnet1
  brctl addif br0 vnet2

  # spuštění dhclienta na bridge
  ip link set dev eth0 up
  ip link flush dev br0
  dhclient -v br0

Nyní již mám připravenou síť a mohu nahodit jednotlivé virtuální stroje.

  # rozkopírování virtuálního disku
  cp deb.qcow2 backend0.qcow2
  cp deb.qcow2 backend1.qcow2
  cp deb.qcow2 ha.qcow2

  # nahození virtuálních strojů (v tmux session)
  kvm -m 1024 -hda backend0.qcow2 -vnc :0 -monitor stdio \
      -net nic,macaddr=54:52:00:00:00:00 -net tap,ifname=vnet0
  kvm -m 1024 -hda backend1.qcow2 -vnc :1 -monitor stdio \
      -net nic,macaddr=54:52:00:00:00:01 -net tap,ifname=vnet1
  kvm -m 1024 -hda ha.qcow2 -vnc :2 -monitor stdio \
      -net nic,macaddr=54:52:00:00:00:02 -net tap,ifname=vnet2

  # nahození všech linků
  ip link set dev vnet0 up
  ip link set dev vnet1 up
  ip link set dev vnet2 up

Po nahození virtuálních serverů a přihlášení přes VNC jsem přenastavil hostname
v /etc/hostname, abych servery rozlišil, nahodil síť z DHCP a nahrál svoje SSH
klíče (ssh-copy-id, ssh-agent).

Příprava prostředí je dokončena.

IP adresy serverů během testování jsou:

  backend0 192.168.11.58
  backend1 192.168.11.95
  ha       192.168.11.25

HA setup
--------

Nastavení HA jsem se rozhodl dělat dvěma způsoby. Pomocí nginx a pomocí haproxy.

Nejprve si rozchodím na obou serverech službu na portu 3128, na které bude
obyčejný nginx s HTML stránkou. Poté budu moci snadno testovat.

  sed -i 's/^\s*listen\s*80/\tlisten 3128/' /etc/nginx/sites-enabled/default
  mkdir /var/www
  cat /etc/hostname > /var/www/index.html
  /etc/init.d/nginx restart

Na hostiteli si pak ověřím dostupnost obou služeb.

  $ curl backend0:3128
  backend0
  $ curl backend1:3128
  backend1

Nyní mohu přistoupit ke konfiguraci HA. Začnu s obyčejným nginx LB. Na ha
serveru nastavím následující.

  cd /etc/nginx/sites-enabled
  rm default
  cat > lb.conf <<EOF
  upstream gdha {
    server backend0:3128;
    server backend1:3128;
  }

  server {
    listen 3128;
    location / {
      proxy_pass http://gdha;
    }
  }
  EOF
  echo '192.168.11.58 backend0' >> /etc/hosts
  echo '192.168.11.95 backend1' >> /etc/hosts
  /etc/init.d/nginx restart

Na ha serveru tak mám nastavený triviální load balancing pro oba backendy. Při
pokusu o sérii přístupů na adresu ha server (192.168.11.25) obdržím občas
odpověd od backend0 a občas od backend1. To neodpovídá zadání, které vyžaduje
active-passive mód. Stačí však pouze lehce upravit konfiguraci HA přidáním
parametru 'backup' k backend1.

  upstream gdha {
    server backend0:3128 fail_timeout=1s max_fails=1;
    server backend1:3128 backup;
  }

Nyní se HA chová podle zadání. Primárně odpovídá backend0. Pokud backend0
odstavím (/etc/init.d/nginx stop), začne požadavky okamžitě obsluhovat backend1
až do té doby, dokud backend0 je opět funkční.

Řešení pomocí nginx je tedy funkční a velmi jednoduché. Je vhodné však pro HTTP
provoz což služba být nemusí. Provedu tedy ještě implementaci pomocí haproxy.

Na ha serveru tedy připravím základní konfiguraci haproxy.

  /etc/init.d/nginx stop
  apt-get install haproxy
  vim /etc/haproxy/haproxy.cfg

V defaultní konfiguraci haproxy nechám jen sekce global a defaults a vytvořím
novou sekci listen s následujícím obsahem. (kompletní konfigurace je k
dispozici v repositáři v adresáří conf/)

  listen  gdha 0.0.0.0:3128
    option  persist
    balance roundrobin
    server  b0 backend0:3128 check inter 2000 fall 3
    server  b1 backend1:3128 check inter 2000 fall 3 backup

Poté haproxy na server ha nastartuji (buď přes init skripty nebo ručně).

  /etc/init.d/haproxy start
  (nebo) haproxy -D -f /etc/haproxy/haproxy.cfg

Při přístupu na ip adresu ha server nyní bude můj požadavek vyřízen backend
serverem 0, trvale. Po odstavení backend0 jsou všechny další požadavky
směrovány na backend1. Po opětovném nahození backend0 jsou ještě chvíli
požadavky zasílány na backend1, dokud nevyprší timeout a poté jsou požadavky
opět zasílány na backend0.

Závěr
=====

Zadání jsem vyřešil pomocí haproxy (+ DNS RR) a dvou backend serverů. Při
řešení i implementaci jsem ignoroval využití AWS popsané v zadání, protože mi
využití AWS dělalo potíže. Řešení je však generické a využitelné prakticky v
libovolném nasazení. Při implementaci jsem také přeskočil datovou část, kterou
bych velmi pravděpodobně implementoval pomocí NFS.

Řešení je velmi jednoduché (na konfiguraci i nasazení), přímočaré (žádné
zbytečné vrstvy), univerzální (službou může být libovolná služba na daném
portu) a škálovatelné (lze přidávat další backendy i HA servery).

Nevýhodou řešení je single point of failure na HA serveru, který pokud samotný
selže a není okamžitě nahrazen alternativním (v rámci DNS RR nebo ruční obnovy
z replik), dojde k úplnému výpadku služby až do obnovení HA serveru na základě
alertů z monitoringu a reakce systémového inženýra. Tato nevýhoda se dá celkem
spolehlivě eliminovat použitím AWS Elastic Load Balancerem.

***

Martin Kopta <martin@kopta.eu>
Wed, 31 Oct 2012 10:07:10 +0100
